qtsystems-opensource-src (5.0~git20230712.81e08ee+dfsg-3) unstable; urgency=medium

  * debian/*.symbols:
    + Fix package names (appending t64) in header lines. (Closes: #1082246).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 22 Sep 2024 12:43:42 +0200

qtsystems-opensource-src (5.0~git20230712.81e08ee+dfsg-2) unstable; urgency=medium

  [ Guido Berhoerster ]
  * debian/patches:
    + Add 1005_check_XOpenDisplay.patch. Check the return value of XOpenDisplay
      and handle failure. This fixes crashes e.g. if Mir/Wayland is used and
      DISPLAY is not set.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 12 Aug 2024 02:58:00 +0200

qtsystems-opensource-src (5.0~git20230712.81e08ee+dfsg-1.1~exp1) experimental; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

 -- Michael Hudson-Doyle <michael.hudson@ubuntu.com>  Thu, 22 Feb 2024 17:24:07 +1300

qtsystems-opensource-src (5.0~git20230712.81e08ee+dfsg-1) unstable; urgency=medium

  * New upstream Git snapshot.
  * debian/copyright:
    + Update copyright attribution for debian/.
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.
    + Replace udev by systemd-dev in B-D. (Closes: #1060526).
  * debian/watch:
    + Add repacksuffix option.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 05 Feb 2024 12:27:42 +0100

qtsystems-opensource-src (5.0~git20190103.e3332ee+dfsg1-5) unstable; urgency=medium

  * debian/control:
    + Add udev/libudev as B-D.

 -- Marius Gripsgard <marius@ubports.com>  Sat, 07 Jan 2023 21:02:19 +0100

qtsystems-opensource-src (5.0~git20190103.e3332ee+dfsg1-4) unstable; urgency=medium

  * Team upload.
  * Update 1004_mark-as-override.patch to add override keyword only to *.h,
    not to *.cpp (closes: #1016274).

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 12 Aug 2022 13:26:26 +0300

qtsystems-opensource-src (5.0~git20190103.e3332ee+dfsg1-2) unstable; urgency=medium

  * debian/patches:
    + Add 1004_mark-as-override.patch. Fix FTBFS (with -Werror) in lomiri-ui-
      toolkit.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 23 Jul 2022 23:16:57 +0200

qtsystems-opensource-src (5.0~git20190103.e3332ee+dfsg1-1) unstable; urgency=medium

  * Add +dfsg1 suffix to the upstream version as we remove jQuery files from the
    upstream code tree.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Dec 2021 21:29:02 +0100

qtsystems-opensource-src (5.0~git20190103.e3332ee-2) unstable; urgency=medium

  * Re-upload merged changes. The previous upload did not contain changes
    of these two uploads: 5.0~git20181230.e3332ee3-3 and
    5.0~git20181230.e3332ee3-4.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Dec 2021 19:15:20 +0100

qtsystems-opensource-src (5.0~git20190103.e3332ee-1) unstable; urgency=medium

  * This upload did not contain the previous two uploads
    (5.0~git20181230.e3332ee3-3, 5.0~git20181230.e3332ee3-4).

  * Re-generate same upstream orig tarball from upstream Git (this time with
    uscan).

  * debian/{rules,copyright,watch}:
    + Use uscan to obtain Git snapshot. Drop self-scripted (thanks to @onlyjob)
      get-orig-source target.
  * debian/copyright:
    + Update copyright attributions.
  * debian/rules:
    + Do dh_auto_install in --no-parallel mode. Build problems have been
      observed when running dh_auto_install parallelly.
  * debian/*.symbols:
    + Update .symbols files.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Dec 2021 18:23:15 +0100

qtsystems-opensource-src (5.0~git20181230.e3332ee3-4) unstable; urgency=medium

  * Team upload.
  * Apparently there are issues when running the tests in parallel, so
    re-introduce a debian/run-tests.sh script, albeit that simply calls
    dh_auto_test --no-parallel now. This is needed because dbus-test-runner
    does not support passing parameters to the task. Thanks Mike Gabriel for
    the notice (and grr to dbus-test-runner for the limitation).
  * Bump the dbus-test-runner timeout from 300 seconds to to 600 seconds,
    which should be enough for more tests in a sequential way.
  * Simplify the "export && export ... && command" chain to use env instead.
  * Update the symbols files from the logs of buildds.

 -- Pino Toscano <pino@debian.org>  Tue, 21 Apr 2020 13:54:26 +0200

qtsystems-opensource-src (5.0~git20181230.e3332ee3-3) unstable; urgency=medium

  * Team upload.
  * Restrict the libbluetooth-dev, and libevdev-dev build dependencies as
    linux-any, as they exist only on Linux.
  * Remove few hardcoded library dependencies, in case they are unversioned
    (they will get a proper version via symbols).
  * Move bits of private development files from qtsystems5-dev to
    qtsystems5-private-dev, where they belong
    - add proper breaks/replaces
  * Enable the udisks config only on Linux, as udisks exists only on Linux.
  * Update the symbols files from the logs of buildds.
  * Pass QT_BUILD_PARTS+=tests to qmake to directly build the tests.
  * Simplify the execution of tests:
    - do not build the tests again, since they are built already
    - call directly dh_auto_test instead of run-tests.sh, as all the script
      does is to wrap 'make check'
    - drop debian/run-tests.sh, no more needed now
  * Do not manually remove files under debian/tmp in the dh_clean override,
    as that will be done by dh_clean.

 -- Pino Toscano <pino@debian.org>  Tue, 21 Apr 2020 10:43:28 +0200

qtsystems-opensource-src (5.0~git20190103.e3332ee-1) unstable; urgency=medium

  * Re-generate same upstream orig tarball from upstream Git (this time with
    uscan).

  * debian/{rules,copyright,watch}:
    + Use uscan to obtain Git snapshot. Drop self-scripted (thanks to @onlyjob)
      get-orig-source target.
  * debian/copyright:
    + Update copyright attributions.
  * debian/rules:
    + Do dh_auto_install in --no-parallel mode. Build problems have been
      observed when running dh_auto_install parallelly.
  * debian/*.symbols:
    + Update .symbols files.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Dec 2021 18:23:15 +0100

qtsystems-opensource-src (5.0~git20181230.e3332ee3-2) unstable; urgency=medium

  * debian/control:
    + Disable B-D on libmirclient-dev until it's available in Debian.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 20 Apr 2020 14:26:22 +0200

qtsystems-opensource-src (5.0~git20181230.e3332ee3-1) unstable; urgency=medium

  [ Timo Jyrinki ]
  * Initial release. (Closes: #953959).
  * debian/patches/build_with_udisk.patch:
    - Build with udisks support.
  * debian/patches/skip_failing_tests.patch:
    - Skip failing unit tests to allow the rest to be run during build.

  [ Mike Gabriel ]
  * debian/control:
    + Add B-D: libmirclient-dev.
  * debian/patches:
    + Rename patches, following a certain patch naming scheme, see
      debian/patches/README.
    + Add / update patch headers.
  * debian/patches: Add 1003_typo-fixes.patch.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation. Bump DH compat level to version 12.
  * debian/{control,rules}:
    + Drop qtsystems5-dbg dbg:pkg.
  * debian/*.install:
    + Update file lists to be installed.
  * debian/rules:
    + Run tests inside a dbus-test-runner task.
    + Move --fail-missing into dh_missing override.
    + Add get-orig-source target facilitating Git snapshooting.
    + Drop 'mkdir .git'. The .git folder triggers running syncqt.pl (which
      we implemented in get-orig-source target manually, instead).
    + Use rm with wildcards, not with DEB_ variables.
    + Include architecture.mk.
    + Drop minified jquery.js files from upstream files.
    + Honour DEB_BUILD_OPTIONS in dh_auto_test override.
    + Drop now default --parallel flag from DH call.
    + Don't call qmake directly.
    + Set all hardening build flags.
  * debian/control:
    + Add B-D: dbus-test-runner.
    + Update Vcs-*: fields. Packaging now takes place on salsa.debian.org.
    + Bump Standards-Version: to 4.5.0. No changes needed.
    + Add myself to Uploaders: field.
    + Update Homepage: URL (now with https).
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Add further B-Ds: qtbase5-dev, libbluetooth-dev, libevdev-dev, libx11-dev
      (to enable further build features).
    + Add Debian UBports Team to Uploaders: field.
  * debian/copyright:
    + Update Source: field.
    + Use secure URL in Format: field.
    + Rewrite copyright file. Detailed style.
    + Add auto-generated copyright.in template file.
  * debian/*.symbols:
    + Add .symbols files.
  * debian/qtsystems5-examples.lintian-overrides:
    + Drop file. Not needed.
  * debian/watch:
    + Add file, pointing to the location where we may some time find release
      tarballs of qtsystems.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 15 Mar 2020 08:02:31 +0100
